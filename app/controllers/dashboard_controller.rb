class DashboardController < ApplicationController
  def home
  end

  def articles
    @articles = Article.all
  end

  def new_article
    @article = Article.new
    render 'dashboard/article/new_article'
  end
end
