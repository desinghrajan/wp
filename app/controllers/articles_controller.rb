class ArticlesController < ApplicationController
  def create
    @article = Article.new(article_params)
    if @article.save
      redirect_to root_path
    else
      render 'dashboard/article/new_article'
    end
  end

  private
  def article_params
    params.require(:article).permit(:title, :body)
  end
end