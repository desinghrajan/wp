# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Article.create(:title=>'Hello world',:body=>"Just a testing article")
Article.create(:title=>'This is fun',:body=>"Hope you enjoy the fun")
Article.create(:title=>'So fast and quick',:body=>"You are so brilliant")